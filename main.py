#!/usr/bin/env python3

"""
Script for purging old pipelines from a GitLab repo, including their jobs.
Before doing the actual deletion, it lists the pipelines that would be
deleted and asks for confirmation.

Requirements:
- python-gitlab : Install it using "pip install python-gitlab"
- "Owner" permissions to the GitLab repository. See below.

Usage:
1. Update the configuration in the script below.
2. Run the script. Currently all configuration is in the script.
"""

from datetime import datetime,timezone
import dateutil.parser
from dateutil.relativedelta import relativedelta
import gitlab
import os

# config_path has to point to a python-gitlab config file. It should contain
# the repository server and a token with "Owner" permissions and "api" scopes.
# See https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-file-format
config_path = os.path.expanduser('~/.python-gitlab.cfg')
# Pick a configuration from the config file here:
gl = gitlab.Gitlab.from_config('multiarray', [config_path])
# Set the project here:
project = gl.projects.get('mnijhuis-tos/multiarray')
# Set a delta value here. All pipelines older than the delta are deleted.
delta = relativedelta(years=1)

print("Marking for deletion:")
ids = []
now = datetime.now(timezone.utc)
for pipeline in project.pipelines.list(iterator=True):
    updated_at = dateutil.parser.parse(pipeline.updated_at)
    if updated_at + delta < now:
        print(f"Pipeline {pipeline.id} updated at {pipeline.updated_at}")
        ids.append(pipeline.id)

if not ids:
    print("No old pipelines found!")
else:
    answer = input("Type 'yes' to confirm deleting these pipelines: ")
    if (answer == "yes"):
        for id in ids:
            print(f"Deleting pipeline {id}...")
            project.pipelines.get(id).delete()
    else:
        print("Pipelines were not deleted")