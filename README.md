# Gitlab Purge Jobs

Small utility script to remove old job logs from a Gitlab repository

### Prerequisites

1. Python & virtualenv
2. "Owner" permissions to the GitLab repository
3. A token with "api" scope.

### Setup & Install

```shell
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```


### Modify script:

config_path has to point to a python-gitlab config file. It should contain
the repository server and a token with "Owner" permissions and "api" scopes.
See https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-file-format

```python
config_path = os.path.expanduser('~/.python-gitlab.cfg')
```


### Running the script

```shell
python3 main.py
```